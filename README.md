# CadQuery editor flatpak

A flatpak for [CadQuery editor](https://github.com/CadQuery/CQ-editor), based on [OCE](https://github.com/tpaviot/oce/).

OCCT-based version is also available [here](https://framagit.org/flatpaks/org.cadquery.cq-editor-occt).

**This project is a work in progress, don't expect anything for now.**

## Usage

Once installed, type `flatpak run org.cadquery.cq-editor-occt` to run CadQuery editor.

## Reporting

Issues concerning the flatpak should be reported on the issue tracker linked to this repository.

Issues concerning the CadQuery editor should be reported [here](https://github.com/CadQuery/CQ-editor/issues).

Issue concerning the CadQuery framework should be reported [here](https://github.com/CadQuery/cadquery/issues).

## License

Both CadQuery editor and this flatpak are published under [Apache Public v2.0 license](./LICENSE).

